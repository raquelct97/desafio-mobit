#!/bin/bash

#######################################################################################
## System monitor script ##
## Author: Raquel Teixeira (raquelct97@gmail.com)
## Description: Monitors the system disk usage, RAM usage and CPU temperature and if 
##              they are above the established limits sends an email and saves the 
##              log in a file.

# Filename: sytem_monitor.sh 
# Optional arguments: disk_limit_in_% memory_limit_in_% cpu_limit_in_celcius 

# Dependecies: mailutils, use sudo apt-get install mailutils

#######################################################################################

## Log file variable
log_file="sytem_monitor.log"

## Limit variables
disk_limit=${1:-80}                                                             # disk_limit is the frist argument or if not set is 80
ram_limit=${2:-60}                                                              # ram_limit is the second argument or if not set is 80
temp_limit=${3:-50}                                                             # temp_limit is the third argument or if not set is 50

## Mail variables
subject="System Monitor Alert"                                                  ## email subject 
to="raquelct97@gmail.com"                                                       ## sending mail to
email_content=""

echo "#### System Monitor ####"

############### Check disk usage ####################
echo -e "\n\n## Disks ##"          
while read -r -a line; do                                                       # Loop to read command output line by line
    disk=${line[$((0))]}                                                        # Save disk name that is the frist column in the output
    disk_percentage="${line[$((4))]}"                                           # Save usage percentage that is the 4 column in output and remove the % character 

    echo "Disk usage o disk $disk: $disk_percentage%"    

    if (( disk_percentage > disk_limit ))                                       # Compare to see if disk usage is grater than limit set disk_limit
    then
        message="Warning, disk space is running low! $disk - $disk_percentage%" # Generate alert message
        echo -e $message | mail -s $subject $to
        echo -e $(date -u) $message >> $log_file                                # Save the alert in log file
    fi
done < <(df | grep -E '^/dev/' | sed 's/%//g')                                  # Use df to get disk info and filter only the /dev/*


############### Check RAM usage ###################
echo -e "\n\n## RAM Memory ##" 
used_mem=$(free -mt | grep Total | awk '{print $3}')                            # Get amount of ram used by reading the free command output 
total_mem=$(free -mt | grep Total | awk '{print $2}')                           # Get total of ram used by reading the free command output

mem_percentage=$(expr 100 \* $used_mem / $total_mem)                            # Calculate the percentage

echo "Memory usage: $mem_percentage%"

if (( mem_percentage > ram_limit ))                                             # Compare to see if ram usage is grater than limit set ram_limit
then
    message="Warning, Memory is running low! $mem_percentage% used"             # Generate alert message
    echo -e $message | mail -s $subject $to
    echo -e $(date -u) $message >> $log_file                                    # Save the alert in log file
fi
   
########### Check CPU temperature #################
echo -e "\n\n## CPU Temperature ##"
core=0
while read -r -a line; do                                                       
    core=$(expr $core + 1)                                                      # Count each core
    core_temp=$(expr $line / 1000)                                              # Get the temperature in °Celsius
    echo "Core $core temperature: $core_temp °C"

    if (( core_temp > temp_limit ))                                             # Compare to see if the temperature is grater than limit set ram_limit
    then
        message="Warning, CPU $core is running with high temperature! $core_temp °C"  # Generate alert message
        echo -e $message | mail -s $subject $to
        echo -e $(date -u) $message >> $log_file                                # Save the alert in log file
    fi
done < <(cat /sys/class/thermal/thermal_zone*/temp)                             # Copy the content of the system temperature monitor file

