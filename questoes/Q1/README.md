MOBIT - Teste de nível para Projetista de Sistemas Embarcados
==============================================================

# Questão 1 - Script monitor de recursos
Monitora a porcentagem de utilização de todas as partições (/dev/\*), o uso de memória RAM e a temperatura dos cores do sistema e envia um e-mail de alerta para cada variável monitorada que estiver acima dos limites estabelecidos dentro do script. O programa gera um log quando o uso estiver acima do normal.

Nome do script: 

    system_monitor.sh

---------
# Como executar:
    
    ./system_monitor.sh

Caso necessário alterar as permissões do arquivo com o comando:

    chmod +x system_monitor.sh

---------
# Argumentos opcionais de execução:
    
1. **disk_limit**: Limite para gerar alerta de uso de disco em %, padrão é 80
2. **memory_limit**: Limite para gerar alerta de uso de memória RAM em %, padrão é 60
3. **cpu_limit**: Limite para gerar alerta de temperatura da CPU em °C, padrão é 50

---------
# Adicionar ao crotab:
Rodar o script a cada 5 minutos com os limites 60%, 50% e 60°C

    crontab -e
    */5  * * * * /path/.../system_monitor.sh 60 50 60

---------
# Dependencias:
**mailutils**: 

    sudo apt-get install mailutils

---------
# Info:
Autora: Raquel Teixeira
Email: raquelct97@gmail.com

