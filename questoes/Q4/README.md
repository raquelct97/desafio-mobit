MOBIT - Teste de nível para Projetista de Sistemas Embarcados
==============================================================

### 4. Comunicação inter processos
O processo *p_writer* é responsável por realizar a leitura da imagem *data/image.png* e envia-la para o processo *p_reader* que salva a imagem recebida em um novo arquivo *data/output.png*.

A comunicação entre os dois processos é feita através de uma área compartilhada de mémoria e a sincronização é feita por semáfaros nomeados.

---------
# Requisitos:

* gcc 7.4.0 ou superior

---------
# Como executar:
    
    make
    ./pw | ./pr

*Ou pode também ser executado em janelas separadas.*

* Gera uma imagem de saida na pasta data:

        data/output.png

* Para limpar o binário de execução:

        make clean

---------
# Bibliotecas utilizadas:

* stb_image:

        https://github.com/nothings/stb

---------
# Info:
Autora: Raquel Teixeira
Email: raquelct97@gmail.com

