/**
 * @file p_reader.c
 * @author Raquel Teixeira (raquelct97@gmail.com)
 * @brief Receive image from writer and genarates an output image from the received data
 * @version 0.1
 * @date 2021-08-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "definitions.h"

/**
 * @brief Initialize shered memory area
 * 
 * @return void* pointer to the frist shm address
 */
void* initialize_shm(){
    // get shared memory file descriptor
	int fd = shm_open(FD_NAME, O_RDONLY, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		perror("open");
		exit(1);
	}

	// map shared memory to process address space
	void* addr = mmap(NULL, SHM_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		perror("mmap");
		exit(1);
	}
    return addr;
}

/**
 * @brief Generates a new image file from the data of an Image structure
 * 
 * @param img Image structure that holds the data of the original file
 */
void process_image(Image *img){
    int stride = img->image_info[1] * img->image_info[3];
    stbi_write_png(OUTPUT_IMAGE_FILE, img->image_info[1], img->image_info[2], img->image_info[3], img->content, stride);
}

void main()
{
    // Create image structure
    Image image;

    // Initialize semaphores
    sem_t *sem_read = sem_open(RP_SEMAPHORE, O_CREAT, S_IRWXU, 0);
    sem_t *sem_write = sem_open(WP_SEMAPHORE, O_CREAT, S_IRWXU, 0);
    if (sem_read == SEM_FAILED || sem_write == SEM_FAILED) {
        perror("Could not open semaphores!");
        exit(1);
    }

    // Wait for data to be ready
    sem_wait(sem_read);
    // Initialize shared memory
	void* addr = initialize_shm();
    // Copy the shm data to the local structure
	memcpy(image.image_info, addr, sizeof(image.image_info));
    // Unblock shm for writer 
    sem_post(sem_write);
    printf("Recieved info\n");
    for (int i=0; i<INFO_SIZE; i++){
        printf("%d\n", image.image_info[i]);
    }

    char content[image.image_info[0]];

    // Read image content from shm to local buffer
    for (int i=0; i < (image.image_info[0]/SHM_SIZE); i++) {
        sem_wait(sem_read);                                       // Wait the data be ready to read
        memcpy(&content[i*SHM_SIZE], addr, SHM_SIZE);             // Copy data
        sem_post(sem_write);                                      // Unblock shm for Writer
        printf("Recieved content %d\n", i);
    }

    // If any data left get it
    if (image.image_info[0] % SHM_SIZE){
        sem_wait(sem_read);                                         // Wait the data be ready to read
        // Copy data
        memcpy(&content[image.image_info[0] - (image.image_info[0] % SHM_SIZE)], addr, image.image_info[0] % SHM_SIZE);
        sem_post(sem_write);                                        // Unblock shm for Writer
    }

    // Pass content to Image structure
    image.content = content;
    // Generate the output image
	process_image(&image);

    exit(0);
}