/**
 * @file p_writer.c
 * @author Raquel Teixeira (raquelct97@gmail.com)
 * @brief Read an image input and communicate with reader through shared memory
 * @version 0.1
 * @date 2021-08-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "definitions.h"

/**
 * @brief Initialize shered memory area
 * 
 * @return void* pointer to the frist shm address
 */
void* initialize_shm(){
    // get shared memory file descriptor
	int fd = shm_open(FD_NAME, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		perror("open");
		exit(1);
	}

	// extend shared memory object as by default it's initialized with size 0
	int res = ftruncate(fd, SHM_SIZE);
	if (res == -1) {
		perror("ftruncate");
		exit(1);
	}

	// map shared memory to process address space
	void *addr = mmap(NULL, SHM_SIZE, PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		perror("mmap");
		exit(1);
	}
    return addr;
}

/**
 * @brief Close the shared memory area and file descriptor
 * 
 * @param addr address of the mapped shared memory 
 */
void close_shm(void* addr){
    // mmap cleanup
	int res = munmap(addr, SHM_SIZE);
	if (res == -1) {
		perror("munmap");
		exit(1);
	}

	// shm_open cleanup
	int fd = shm_unlink(FD_NAME);
	if (fd == -1) {
		perror("unlink");
		exit(1);
	}
}

/**
 * @brief Read input image file
 * 
 * @param img Image structure that will hold the data of the original file
 */
void read_image(Image *img){
    img->content = stbi_load(INPUT_IMAGE_FILE, &img->image_info[1], &img->image_info[2], &img->image_info[3], 0);
    img->image_info[0] = img->image_info[1] * img->image_info[2] * img->image_info[3];
    if(img == NULL) {
        printf("Error in loading the image\n");
        exit(1);
    }
}

int main() {

    // Read original image
    Image image;
    read_image(&image);

    // Initialize semaphores
	sem_t *sem_read = sem_open(RP_SEMAPHORE, O_CREAT, S_IRWXU, 0);
    sem_t *sem_write = sem_open(WP_SEMAPHORE, O_CREAT, S_IRWXU, 0);
    if (sem_read == SEM_FAILED || sem_write == SEM_FAILED) {
        perror("Could not open semaphores!");
        exit(1);
    }

    // Initialize shared memory
    void *addr = initialize_shm();

    // Copy the image info to the shared memory area
	memcpy(addr, image.image_info, sizeof(image.image_info));
    // Unblock Data to reader 
    sem_post(sem_read);
    printf("Sent info\n");

    // Copy Image content to shm in chunks of SHM_SIZE
    for (int i=0; i < (image.image_info[0]/SHM_SIZE); i++) {
        sem_wait(sem_write);                                    // Wait reader to get data              
        memcpy(addr, &image.content[i*SHM_SIZE], SHM_SIZE);     // Copy data to shm
        sem_post(sem_read);                                     // Unblock Data to reader 
        printf("Sent content %d\n", i);
    }

    // If any data left send the rest of it
    if (image.image_info[0] % SHM_SIZE){
        sem_wait(sem_write);                                    // Wait reader to get data
        // Copy the data     
        memcpy(addr, &image.content[image.image_info[0] - (image.image_info[0] % SHM_SIZE)], image.image_info[0] % SHM_SIZE);
        sem_post(sem_read);                                     // Unblock Data to reader
    }

    // Wait reader to get data
    sem_wait(sem_write);
    // Closes the shared memory
	close_shm(addr);
    exit(0);
}
