/**
 * @file definitions.h
 * @author Raquel Teixeira (raquelct97@gmail.com)
 * @brief Definitions for reader and writer processes
 * @version 0.1
 * @date 2021-08-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <fcntl.h>

#define STB_IMAGE_IMPLEMENTATION
#include "../lib/stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../lib/stb_image/stb_image_write.h"

#define INPUT_IMAGE_FILE "data/image.jpg"
#define OUTPUT_IMAGE_FILE "data/output.png"

#define FD_NAME "/shm_writer"                   // Shared memory File descriptor name
#define SHM_SIZE (1024*1024)                    // Shared memory size
#define INFO_SIZE 4                             // Size of image info array

#define RP_SEMAPHORE "semaphore_r"               //Read semaphore name
#define WP_SEMAPHORE "semaphore_w"               //Write semaphore name

// Image structure
typedef struct image {
    unsigned char *content;                     // Image content array
    int image_info[INFO_SIZE];                  // Image info array [0]-size, [1]-width, [2]-heigth, [3]-channels
} Image;