#include <iostream>
#include <vector>
#include <thread>
#include <syslog.h>
#include <mutex> 

std::mutex mtx;           // mutex for critical section

static const int THREAD_COUNT = 10;

static void print_log(int id)
{
    mtx.lock();             // critical section (exclusive access to output signaled by locking mtx)
    syslog (LOG_INFO,"--------------------\n");
    syslog (LOG_INFO,"Iniciando bloco %d\n",id);
    syslog (LOG_INFO,"Hello world from thread %d\n",id);
    syslog (LOG_INFO,"Fim do bloco %d\n", id);
    syslog (LOG_INFO,"--------------------\n");
    mtx.unlock();
}


int main()
{
    std::vector<std::thread> v;

    for (size_t i = 0; i < THREAD_COUNT; i++)
    {
        v.emplace_back(print_log, i);
    }

    for (auto &t : v)
    {
        t.join();
    }

    return 0;
}