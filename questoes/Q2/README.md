MOBIT - Teste de nível para Projetista de Sistemas Embarcados
==============================================================

# Questão 2 - Sincronizando o uso do log com várias threads
    
Inicializar várias threads, cada uma loga uma mensagem no *syslog* e são sincronizadas via mutex.

---------
# Requisitos:

* g++ 7.4.0 ou superior

---------
# Como executar:
    
    make
    ./q2

Para vizualizar a saida basta ver o conteúdo do syslog:

    tail -f /var/log/syslog

Para limpar o binário de execução:

    make clean

---------
# Info:
Autora: Raquel Teixeira
Email: raquelct97@gmail.com

