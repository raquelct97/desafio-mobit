MOBIT - Teste de nível para Projetista de Sistemas Embarcados
==============================================================

### 3. Produtor-Consumidor em C++

1 - O programa conta com duas threads independentes: Produtora e Consumidora

2 - A thread Produtora lê o arquivo input.xml e envia seu conteúdo para uma fila, de onde a thread Consumidora lê e imprime na tela apenas o conteúdo que estiver dentro das tags `<payload></payload>`. A fila é sincronizada através de mutex e variável condicional.

---------
# Requisitos:

* g++ 7.4.0 ou superior

---------
# Como executar:
    
    make
    ./q3

* Para limpar o binário de execução:

        make clean

---------
# Bibliotecas utilizadas:

* tinyxml:

        https://github.com/leethomason/tinyxml2

---------
# Info:
Autora: Raquel Teixeira
Email: raquelct97@gmail.com

