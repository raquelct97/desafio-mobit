/**
 * @file q3.cpp
 * @author Raquel Teixeira (raquelct97@gmail.com)
 * @brief Multithread queue sync program to read a xml file
 * @version 0.1
 * @date 2021-08-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <string>
#include "../lib/tinyxml/tinyxml2.cpp"
#include "queue.cpp"
#include <fstream>

#define INPUT_FILE "input.xml"  // Inpute file name

using namespace std;
using namespace tinyxml2;

/**
 * @brief Reads a XML file as std:string and put it to the queue
 * 
 * @param queue SyncQueue structure
 */
void producer_function(SyncQueue* queue) {

    // Read the file stream
    std::ifstream ifs(INPUT_FILE);
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));
                       
    
    // Put it on the queue
    queue->enque(content);
}

/**
 * @brief Get the message from the queue and parse the xml to print only the data in the payload tag
 * 
 * @param queue SyncQueue structure
 */
void consumer_function(SyncQueue* queue) {

    XMLDocument doc;
    std::string message;
    
    // Read the queue
    message = queue->deque();

    // Converts to const char*
    const char *msg = message.c_str();

    // Parse the xml text to an XMLDocument structure
    doc.Parse(msg);
    // Get the <payload> tag text  
    const char* parsed_xml = doc.FirstChildElement("Mensagem")->FirstChildElement("payload")->GetText();

    std::cout << "Recieved message: " << std::endl;
    std::cout << parsed_xml << std::endl;
}

int main() {
    // Initialize shared queue
    SyncQueue queue;

    // Initialize threads
    std::thread producer(producer_function, &queue);
    std::thread consumer(consumer_function, &queue);

    // Wait for them to finnish
    producer.join();
    consumer.join();

    return 0;
}