/**
 * @file queue.cpp
 * @author Raquel Teixeira (raquelct97@gmail.com@gmail.com)
 * @brief SyncQueue class definition
 * @version 0.1
 * @date 2021-08-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

class SyncQueue
{
    //Attributes of the SyncQueue
    std::queue<std::string> m_Que;      // Queue object
    std::mutex m_Lock;                  // Mutex object
    std::condition_variable m_ConVar;   // Condition variable object

public:
    /**
     * @brief Put an std::string item in the queue
     * 
     * @param item string to put in the queue
     */
    void enque(std::string item)
    {
        // Lock the queue 
        std::unique_lock<std::mutex> lock(m_Lock);
        // Push the item in the queue
        m_Que.push(item);
        // Unlock the queue an notify all threads waiting
        lock.unlock(); 
        m_ConVar.notify_all();
    }

    /**
     * @brief Get an item from the queue
     * 
     * @return std::string - the first string in the queue
     */
    std::string deque()
    {
        // Lock the queue 
        std::unique_lock<std::mutex> lock(m_Lock);

        // Wait for to be an item to read
        do
        {
            m_ConVar.wait(lock);

        } while(m_Que.size() == 0); // extra check from spontaneous notifications

        // Pop the frist element in the queue
        auto ret = m_Que.front();
        m_Que.pop();

        return ret;
    }
};